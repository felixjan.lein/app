package app.listener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

/**
 * GrayscaleListener Klasse horcht auf den Grayscale Button
 */
public class GrayscaleListener implements ActionListener {
    /**
     * Objektvariable img speichert das Bild
     */
    private final BufferedImage img;
    /**
     * label speichert die Referenz auf das ursprüngliche Label
     */
    private final JLabel label;

    /**
     * Konstruktor für einen GrayscaleListener
     * @param image das Bild
     * @param imageLabel Referenz auf das Label
     */
    public GrayscaleListener(BufferedImage image, JLabel imageLabel){
        this.img = image;
        this.label = imageLabel;
    }

    /**
     * Wird ausgeführt, wenn Grayscale Knopf gedrückt
     * @param e event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Icon icon = new ImageIcon(getGrayscaleBuf(img));
        label.setIcon(icon);
    }

    /**
     * Die Funktion bekommt ein BufferedImage und macht es grau.
     * Es wird skaliert und grau gemacht.
     * @param img Das zu entfärbende Bild
     * @return Ein neues Buffered Image, das Ergebnis
     */
    private static BufferedImage getGrayscaleBuf(BufferedImage img){
        int w = img.getWidth();
        int h = img.getHeight();

        BufferedImage result = new BufferedImage(w*2, h*2, img.getType());

        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                int c = img.getRGB(x, y);
                Color color = new Color(c);

                int newC = (color.getRed() + color.getGreen() + color.getBlue()) / 3;
                Color newColor = new Color(newC, newC, newC);

                result.setRGB(x*2, y*2, newColor.getRGB());
                result.setRGB(x*2, y*2+1, newColor.getRGB());
                result.setRGB(x*2+1, y*2, newColor.getRGB());
                result.setRGB(x*2+1, y*2+1, newColor.getRGB());
            }
        }
        return result;
    }
}
