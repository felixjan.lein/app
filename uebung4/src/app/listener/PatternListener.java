package app.listener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

/**
 * PatternListener Klasse horcht auf den Pattern Button
 */
public class PatternListener implements ActionListener {
    /**
     * Objektvariable img speichert das Bild
     */
    private final BufferedImage img;
    /**
     * label speichert die Referenz auf das ursprüngliche Label
     */
    private final JLabel label;

    /**
     * Konstruktor für einen PatterListener
     * @param image das Bild
     * @param imageLabel Referenz auf das Label
     */
    public PatternListener(BufferedImage image, JLabel imageLabel){
        this.img = image;
        this.label = imageLabel;
    }

    /**
     * Wird ausgeführt, wenn Pattern Knopf gedrückt
     * @param e event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Icon icon = new ImageIcon(getPatternBuf(img));
        label.setIcon(icon);
    }

    /**
     * Die Funktion bekommt ein BufferedImage und wendet das Pattern darauf an.
     * Es wird skaliert und die 2*2 Pixel werden durch ein Pattern ersetzt.
     * @param img Das zu verändernde Bild
     * @return Ein neues Buffered Image, das Ergebnis
     */
    private static BufferedImage getPatternBuf(BufferedImage img){
        int w = img.getWidth();
        int h = img.getHeight();

        BufferedImage result = new BufferedImage(w*2, h*2, img.getType());

        //true == WHITE // false == BLACK
        boolean[][] pattern0 = {{false,false},{false,false}};
        boolean[][] pattern1 = {{true,false},{false,false}};
        boolean[][] pattern2 = {{false,true},{true,false}};
        boolean[][] pattern3 = {{false,true},{true,true}};
        boolean[][] pattern4 = {{true,true},{true,true}};

        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                int c = img.getRGB(x, y);
                Color color = new Color(c);

                int newC = (color.getRed() + color.getGreen() + color.getBlue()) / 3;


                if(newC >= 208){
                    applyPattern(pattern4, x, y, result);
                } else if(newC >= 156){
                    applyPattern(pattern3, x, y, result);
                } else if(newC >= 104){
                    applyPattern(pattern2, x, y, result);
                } else if(newC >= 52){
                    applyPattern(pattern1, x, y, result);
                } else{
                    applyPattern(pattern0, x, y, result);
                }
            }
        }
        return result;
    }

    /**
     * Die Funktion wendet ein übergebenes Pattern ein eine übergebene Stelle
     * eines übergebenen BufferedImages an, ersetzt also Pixel.
     * @param pattern Das Pattern, welches angewendet werden soll
     * @param x Die x-Koordinate
     * @param y Die y-Koordinate
     * @param result Das BufferedImage auf dem gearbeitet wird
     */
    private static void applyPattern(boolean[][] pattern, int x, int y, BufferedImage result){
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                if (pattern[i][j]){
                    result.setRGB(x*2 + i, y*2 + j, Color.WHITE.getRGB());
                }
            }
        }
    }
}