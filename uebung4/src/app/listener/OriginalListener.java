package app.listener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

/**
 * OriginalListener Klasse horcht auf den Original Button
 */
public class OriginalListener implements ActionListener {
    /**
     * Objektvariable img speichert das Bild
     */
    private final BufferedImage img;
    /**
     * label speichert die Referenz auf das ursprüngliche Label
     */
    private final JLabel label;

    /**
     * Konstruktor für einen OriginalListener
     * @param image das Bild
     * @param imageLabel Referenz auf das Label
     */
    public OriginalListener(BufferedImage image, JLabel imageLabel){
        this.img = image;
        this.label = imageLabel;
    }

    /**
     * Wird ausgeführt, wenn Original Knopf gedrückt
     * @param e event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Icon icon = new ImageIcon(getScaledBuf(img));
        label.setIcon(icon);
    }

    /**
     * Die Funktion bekommt ein BufferedImage und skaliert es mit dem Faktor Zwei
     * Das heißt es wird doppelt so breit und hoch. Ein Pixel wird zu Vier Pixel.
     * @param img Das zu skalierende Bild
     * @return Ein neues Buffered Image, das Ergebnis
     */
    public static BufferedImage getScaledBuf(BufferedImage img){
        int w = img.getWidth();
        int h = img.getHeight();

        BufferedImage result = new BufferedImage(w*2, h*2, BufferedImage.TYPE_INT_ARGB);

        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                int c = img.getRGB(x, y);
                result.setRGB(x*2, y*2, c);
                result.setRGB(x*2, y*2+1, c);
                result.setRGB(x*2+1, y*2, c);
                result.setRGB(x*2+1, y*2+1, c);
            }
        }
        return result;
    }
}
