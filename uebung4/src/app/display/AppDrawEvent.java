package app.display;

import app.listener.CloseListener;
import app.listener.GrayscaleListener;
import app.listener.OriginalListener;
import app.listener.PatternListener;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * Klasse für einen eigenen Button ohne blauen Schimmer
 */
class MyButton extends JButton{
    /**
     * Konstruktor für einen Button
     * @param text Display text
     */
    public MyButton(String text){
        super(text);
        setFocusPainted(false);
    }
}

/**
 * Neuer Konstruktor für einen Frame mit einer DefaultCloseOperation
 */
class AppFrame extends JFrame {
    /**
     * Konstruktor für besagten Frame
     * @param title Titel des Fensters
     */
    public AppFrame(String title) {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

/**
 * Hauptklasse, welche die ausführbare Main-Methode enthällt
 */
public class AppDrawEvent {
    /**
     * Main-Methode, welche ein Bild übergeben bekommt
     * @param args Pfad zu einem Bild
     */
    public static void main( String[] args ) {
        //Wenn nicht genau ein Parameter
        if(args.length != 1){
            throw new IllegalArgumentException("Exactly one image path as input allowed");
        }

        //Neues BufferedImage wird initialisiert
        final BufferedImage inputImage;
        final String path = args[0];
        try {
            inputImage = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        //#### INITIALIZE ####
        JFrame frame = new AppFrame("APP  -  Übung 4  -  Felix Lein");

        JPanel headerPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel imagePanel = new JPanel(new BorderLayout());
        JPanel footerPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        frame.add(headerPanel, BorderLayout.NORTH);
        frame.add(imagePanel, BorderLayout.CENTER);
        frame.add(footerPanel, BorderLayout.SOUTH);

        //#### IMAGE + DESCRIPTION ####
        JLabel imageLabel = new JLabel(new ImageIcon(OriginalListener.getScaledBuf(inputImage)));
        JLabel imageName = new JLabel("image_path=" + path, JLabel.CENTER);
        imagePanel.add(imageLabel, BorderLayout.CENTER);
        imagePanel.add(imageName, BorderLayout.SOUTH);

        //#### EFFECT BUTTONS ###
        JButton original = new MyButton("Original");
        JButton grayscale = new MyButton("Grayscale");
        JButton pattern = new MyButton("Pattern");

        headerPanel.add(original);
        headerPanel.add(grayscale);
        headerPanel.add(pattern);

        original.addActionListener(new OriginalListener(inputImage, imageLabel));
        grayscale.addActionListener(new GrayscaleListener(inputImage, imageLabel));
        pattern.addActionListener(new PatternListener(inputImage, imageLabel));

        //#### EXIT BUTTON ####
        JButton exit = new MyButton("Exit");
        footerPanel.add(exit);
        exit.addActionListener(new CloseListener());


        //#### FINALIZE ####
        frame.pack();
        frame.setVisible(true);
    }
}
