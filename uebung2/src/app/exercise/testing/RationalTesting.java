package app.exercise.testing;

import app.exercise.algebra.Rational;

/**
 * Klasse, welche die Funktionsweise von Objekten der Klasse Rational
 * ausgiebig testet.
 * @author felix
 */
public class RationalTesting {
    /**
     * Main-Methode, welche beim Ausführen der Klasse viele Tests
     * ausgibt, in denen jede Methode von Rational und BasisFraction
     * getestet wird.
     * @param args sollte leer bleiben
     */
    public static void main(String[] args) {
        Rational result;
        int hash;

        //Beide Konstruktoren und setND() kommen zum Einsatz
        Rational r1 = new Rational(1, 6);
        Rational r2 = new Rational(2, -5);
        Rational r3 = new Rational();
        r3.setND(-7, 12);
        Rational r4 = new Rational();
        r4.setND(6,8);

        //toString() kommt zum Einsatz
        System.out.println("Bruch 1 = " + r1);
        System.out.println("Bruch 2 = " + r2);
        System.out.println("Bruch 3 = " + r3);
        System.out.println("Bruch 4 = " + r4);

        //clone kommt oft zum Einsatz
        System.out.println("\n-- Addition --");
        result = r1.clone();
        result.add(r2);
        System.out.println("Ergebnis Bruch 1 + 2 = " + result);
        result = r3.clone();
        result.add(r4);
        System.out.println("Ergebnis Bruch 3 + 4 = " + result);

        System.out.println("\n-- Subtraktion --");
        result = r1.clone();
        result.sub(r2);
        System.out.println("Ergebnis Bruch 1 - 2 = " + result);
        result = r3.clone();
        result.sub(r4);
        System.out.println("Ergebnis Bruch 3 - 4 = " + result);

        System.out.println("\n-- Multiplikation --");
        result = r1.clone();
        result.mul(r2);
        System.out.println("Ergebnis Bruch 1 * 2 = " + result);
        result = r3.clone();
        result.mul(r4);
        System.out.println("Ergebnis Bruch 3 * 4 = " + result);

        System.out.println("\n-- Division --");
        result = r1.clone();
        result.div(r2);
        System.out.println("Ergebnis Bruch 1 / 2 = " + result);
        result = r3.clone();
        result.div(r4);
        System.out.println("Ergebnis Bruch 3 / 4 = " + result);

        System.out.println("\n-- Kehrwert --");
        result = (Rational) r1.reciprocal();
        System.out.println("Kehrwert von Bruch 1 = " + result);
        result = (Rational) r3.reciprocal();
        System.out.println("Kehrwert von Bruch 3 = " + result);

        System.out.println("\n-- Negation --");
        result = (Rational) r2.reciprocal();
        System.out.println("Negation von Bruch 2 = " + result);
        result = (Rational) r4.reciprocal();
        System.out.println("Negation von Bruch 4 = " + result);

        System.out.println("\n-- Hash-Code --");
        hash = r1.hashCode();
        System.out.println("Hash von Bruch 1 = " + hash);
        hash = r3.hashCode();
        System.out.println("Hash von Bruch 3 = " + hash);

        System.out.println("\n-- Getter --");
        System.out.println("Zähler von Bruch 2 = " + r2.getN() + ", und der Nenner = " + r2.getD());
        System.out.println("Zähler von Bruch 4 = " + r4.getN() + ", und der Nenner = " + r4.getD());

        System.out.println("\n-- Vergleich --");
        Rational r5 = r1.clone();
        System.out.println("Bruch 1 und 2 gleich. = " + r1.equals(r2));
        System.out.println("Bruch 1 und sein Clone sind gleich. = " + r1.equals(r5));
    }
}
