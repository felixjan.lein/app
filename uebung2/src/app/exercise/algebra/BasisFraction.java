package app.exercise.algebra;

/**
 * Diese klasse implementiert einige Methoden aus Fractional,
 * zum arbeiten mit Rational Objekten.
 * @author felix
 */
public abstract class BasisFraction implements Fractional {

    /**
     * Methodendeklaration einer Setter-Methode
     * @param numerator Zähler
     * @param denominator Nenner
     */
    protected abstract void setND(long numerator, long denominator);

    /**
     * Diese Methode addiert ein Rational auf das jetzige Objekt
     * @param operand ein Summand Rational
     */
    @Override
    public void add(Fractional operand) {
        long n = this.getN()*operand.getD()+operand.getN()*this.getD();
        long d = this.getD()*operand.getD();
        setND(n,d);
    }

    /**
     * Die Methode gibt die Negation eines Rationals als neues Objekt zurück
     * @return neues Rational Objekt
     */
    @Override
    public Fractional negation() {
        return new Rational(-this.getN(), this.getD());
    }

    /**
     * Diese Methode subtrahiert ein Rational von dem jetzigen Objekt
     * @param operand ein Subtrahend Rational
     */
    @Override
    public void sub(Fractional operand) {
        add(operand.negation());
    }

    /**
     * Diese Methode multipliziert ein Rational auf das jetzige Objekt
     * @param operand ein Faktor Rational
     */
    @Override
    public void mul(Fractional operand) {
        long n = this.getN() * operand.getN();
        long d = this.getD() * operand.getD();
        setND(n,d);
    }

    /**
     * Die Methode gibt ein neues Rational Objekt zurück, unzwar den Kehrwert
     * @return ein neues Rational
     */
    @Override
    public Fractional reciprocal() {
        return new Rational(this.getD(), this.getN());
    }

    /**
     * Die Methode dividiert ein Fractional von dem jetzigen Objekt
     * @param operand ein divisor Rational
     */
    @Override
    public void div(Fractional operand) {
        mul(operand.reciprocal());
    }
}
