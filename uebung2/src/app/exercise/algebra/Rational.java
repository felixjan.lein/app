package app.exercise.algebra;

/**
 * Klasse zum Rechnen mit rationalen Brüchen
 * @author felix
 */
public class Rational extends BasisFraction{

    /**
     * Objektvariable numerator ist der Zähler
     */
    private long numerator;
    /**
     * Objektvariable denominator ist der Nenner
     */
    private long denominator;

    /**
     * Konstruktor mit zwei Parametern
     * @param numerator Zähler
     * @param denominator Nenner
     */
    public Rational(long numerator, long denominator){
        setND(numerator,denominator);
    }

    /**
     * Default Konstruktor, welcher den Bruch 1/1 erzeugt
     */
    public Rational(){
        setND(1,1);
    }

    /**
     * Methode zum Setzen des Zählers und Nenners
     * @param numerator Zähler
     * @param denominator Nenner
     */
    @Override
    public void setND(long numerator, long denominator) {

        long gcd_result = gcd(numerator, denominator);
        long n = numerator / gcd_result;
        long d = denominator / gcd_result;

        if (d < 0){
            d *= -1;
            n *= -1;
        }

        this.numerator = n;
        this.denominator = d;
    }

    /**
     * Getter Methode
     * @return Long, Zähler
     */
    @Override
    public long getN() {
        return this.numerator;
    }

    /**
     * Getter Methode
     * @return Long, Nenner
     */
    @Override
    public long getD() {
        return this.denominator;
    }

    /**
     * greatest common divisor
     * @param a Zahl 1
     * @param b Zahl 2
     * @return Ein Long, der größter gemeinsamer Teiler der beiden Zahlen
     */
    public static long gcd(long a, long b) {
        if (b==0) return a;
        return Math.abs(gcd(b,a%b));
    }

    /**
     * Kompaktere Ausgabe mit eigener toString Methode
     * @return Der Bruch als String in der Form: x/y, oder falls
     * y == 1 nur x.
     */
    @Override
    public String toString() {
        if (this.denominator == 1){
            return Long.toString(this.numerator);
        } else {
            return this.numerator + "/" + this.denominator;
        }
    }

    /**
     * Methode zum Clonen von Objekten
     * @return Ein neues Rational Objekt mit gleichem Zähler und Nenner
     */
    @Override
    public Rational clone(){
        return new Rational(this.getN(), this.getD());
    }

    /**
     * Methode zum vergleichen zweier Objekte
     * @param o Ein Objekt
     * @return boolean ob Zähler und Nenner, sowie die Klasse gleich sind
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (this.numerator != ((Rational) o).numerator) return false;
        return this.denominator == ((Rational) o).denominator;
    }

    /**
     * Methode, welche einen Hash-Code für jedes Objekt erzeugt
     * @return Integer,  irgendwie erzeugter Hash-Code
     */
    @Override
    public int hashCode() {
        return (int) (((numerator + denominator) * 7)<<3)%71;
    }
}
