public class PolyTest {
    public static void test1(){
        //Schöne Kopfzeile
        System.out.println("~Test 1~\ni │ hash │ x^i\n══╪══════╪════════════");
        //Der Generator
        PolynomialGF2 generator = new PolynomialGF2();
        //Das irreduzible Polynom
        PolynomialGF2 irr = new PolynomialGF2(new boolean[] {true, false, true, true});

        //Für jede Zeile wird alles schick geprintet
        for (int i = 0; i < 7; i++) {
            //Das jeweilige Polynom ist der Genrator geshiftet um i
            PolynomialGF2 res = generator.shift(i).mod(irr);
            //Der Hashcode des Polynoms wird ausgegeben und print_pretty des Objekts
            System.out.println(i + " │    " + res.hashCode() + " │ " + print_pretty(res));
        }
    }

    //print_pretty bekommt ein Polynom und returnt einen Schönen String
    public static String print_pretty(PolynomialGF2 p){
        //Das Koeffizienten Arrays
        boolean[] b = p.toArray();
        //Der Ergebnis String
        String res = "";
        //Für jedes Element des Arrays gehen wir von hinten durch
        for (int i = b.length - 1; i >= 0; i--) {
            //Nur wenn das Element true ist wird was hinzugefügt
            if (b[i]){
                if (i == b.length - 1) {
                    //Falls das letzte Element true ist, eine 1
                    res = "1 + %s".formatted(res);
                } else if (i == b.length - 2) {
                    //Falls das vorletzte Element true ist, ein x
                    res = "x + %s".formatted(res);
                } else {
                    //Sonst einfach x^i
                    res = "x^%s + %s".formatted(String.valueOf(b.length - i - 1), res);
                }
            }
        }
        //Das " + " wird entfernt
        if (res.endsWith(" + ")) {
            res = res.substring(0, res.length() - 2);
        }
        //Rückgabe
        return res;
    }

    public static void test2(){

        //Der Generator
        PolynomialGF2 generator = new PolynomialGF2(new boolean[] {true, true});
        //Das irreduzible Polynom
        PolynomialGF2 irr = new PolynomialGF2(new boolean[] {true, false, false, false, true, true, false, true, true});

        //Das Ergebnis Polynom
        PolynomialGF2 res = new PolynomialGF2();
        //Hash-String
        String hash_String;
        //Hash selber
        int hashCode;

        //Schöne Kopfzeile
        System.out.println("\n~Test 2~");
        System.out.println("  │  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f");
        System.out.println("══╪════════════════════════════════════════════════");

        //Für jedes Element
        for(int i=0; i<256; i++) {
            //Eventueller Zeilensprung (Falls nicht erstes Element) und schöne erste Spalte
            if(i%16 == 0) {
                if(i != 0) {
                    System.out.print("\n");
                }
                System.out.print(Integer.toHexString(i / 16) + " │ ");
            }

            //Ergebnis mit Hilfe des Genrator Polynoms berechnen
            if (i != 0){
                res = res.times(generator);
            }

            //Hashcode bilden
            hashCode = res.mod(irr).hashCode();
            //Ihn in einen String schreiben
            hash_String = Integer.toHexString(hashCode) + " ";

            //Falls der Hash Hexadezimal einstellig ist, führende 0
            if(hashCode < 16) {
                hash_String = "0" + hash_String;
            }
            //Ausgabe des Strings
            System.out.print(hash_String);
        }
    }
    public static void main(String[] args) {
        test1();
        test2();
    }
}
