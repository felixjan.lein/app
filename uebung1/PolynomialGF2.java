import java.util.Arrays;

public class PolynomialGF2{

    //Koeffizienten-Array
    final private boolean[] poly;

    //Das One und Zero Polynom
    final static private PolynomialGF2 ONE = new PolynomialGF2(new boolean[] {true});
    final static private PolynomialGF2 ZERO = new PolynomialGF2(null);

    //Default Konstruktor
    public PolynomialGF2() {
        poly = ONE.poly;
    }

    //Konstruktor mit Koeffizienten-Array
    public PolynomialGF2(boolean[] f) {
        if (f == null){
            poly = null;
        } else {
            poly = trim(f);
        }
    }

    //trim Funktion um führenden Falses wegzukürzen
    private static boolean[] trim(boolean[] f) {
        boolean allFalse = true;
        for (boolean b: f) {
            if (b){
                allFalse = false;
                break;
            }
        }
        //Falls nur false, return das null-Polynom
        if(allFalse){
            return null;
        }
        //Anzahl false zählen
        int i = 0;
        while (!f[i]) {
            i++;
        }
        //Neues Array entsprechender Länge
        boolean[] newF = new boolean[f.length - i];
        //kopieren
        System.arraycopy(f, i, newF, 0, f.length - i);
        //Rückgabe des neuen Arrays
        return newF;
    }

    //Guckt ob das Array des Polynoms, gleich ist mit ZERO
    public boolean isZero(){
        return Arrays.equals(this.poly, ZERO.poly);
    }
    //Guckt ob das Array des Polynoms, gleich ist mit ONE
    public boolean isOne(){
        return Arrays.equals(this.poly, ONE.poly);
    }

    //Gibt das Poly-Array zurück
    public boolean[] toArray(){
        return this.poly;
    }

    //Neue toString-Methode
    @Override
    public String toString() {
        //Falls null, entsprechende Ausgabe
        if (poly != null) {
            return "n=" + poly.length + " - koeff=" + Arrays.toString(poly);
        }
        return "n=0 - koeff=null";
    }


    //clone returnt ein neues Polynom mit gleichem Feld
    @Override
    protected PolynomialGF2 clone(){
        return new PolynomialGF2(this.poly);
    }

    //equals guckt ob die Klassen gleich sind und ob die Arrays übereinstimmen
    @Override
    public boolean equals(Object o) {
        if (getClass() != o.getClass()){
            return false;
        }
        return Arrays.equals(poly, ((PolynomialGF2) o).poly);
    }

    //Hashcode setzt die gegebene Summenbildung um
    @Override
    public int hashCode() {
        int hash = 0;
        for (int i = 0; i < poly.length; i++) {
            if (poly[i]) hash = hash + (int) Math.pow(2, poly.length - i -1);
        }
        return hash;
    }

    //Addition zweier Polynome
    public PolynomialGF2 plus(PolynomialGF2 p){
        //Alle Edge-Cases wo entweder eins das Null-Polynom ist oder beide
        if (this.equals(ZERO)) {
            if (!p.equals(ZERO)){
                return p;
            } else {
                return ZERO;
            }
        } else{
            if (p.equals(ZERO)){
                return this;
            }
        }

        //len ist die maximale Array-Länge
        int len = Math.max(poly.length, p.poly.length);
        //diff ist die Differenz der beiden Polynome
        int diff = len - Math.min(poly.length, p.poly.length);
        //res ist das Lösungsfeld
        boolean[] res = new boolean[len];

        //Fallunterscheidung ob das eine länger ist, oder das andere
        if(poly.length >= p.poly.length){
            //Das längere wird in die Lösung kopiert
            System.arraycopy(poly, 0, res, 0, poly.length);
            //Auf die Stellen wo auch das andere Stattfindet wird XOR angewendet
            for (int i = 0; i < p.poly.length; i++) {
                res[diff + i] = res[diff + i] ^ p.poly[i];
            }
        } else{
            System.arraycopy(p.poly, 0, res, 0, p.poly.length);
            for (int i = 0; i < poly.length; i++) {
                res[diff + i] = res[diff + i] ^ poly[i];
            }
        }
        //Neues Ergebnis Polynom wird zurückgegeben
        return new PolynomialGF2(res);
    }

    //Multiplikation zweier Polynome
    //Multiplikation ist nur shiften und addieren
    public PolynomialGF2 times(PolynomialGF2 p){
        //standard Ergebnis
        PolynomialGF2 res = ZERO;
        for (int i = 0; i < p.poly.length; i++) {
            if(p.poly[i]){
                //Die einzelnen Summanden sind die geschifteten Polynome, entsprechende
                //des Grades der Polynome des einen Faktors
                PolynomialGF2 add = (this.shift(p.degree()-i));
                res = res.plus(add);
            }
        }
        //Das result Polynom wird zurückgegeben
        return res;
    }
    //Modulo funktioniert ähnlich wie times
    public PolynomialGF2 mod(PolynomialGF2 m){
        PolynomialGF2 res = this.clone();
        //Falls der Grad des Ergebnisses noch höher oder gleich dem Des Modulo ist...
        while (res.degree() >= m.degree()){
            //Wird das Ergebnis um die Differenz der Grad geshiftet und
            //auf das vorherige Ergebnis draufaddiert
            int diff = res.degree() - m.degree();
            res = res.plus(m.shift(diff));
        }
        return res;
    }

    //Degree returnt den Grad
    private int degree(){
        if(this.poly == null){
            return 0;
        } else {
            return this.poly.length - 1;
        }
    }

    //Shift fügt degree viele False ans Ende an
    public PolynomialGF2 shift(int degree){
        //default Füllung mit false
        boolean[] result = new boolean[poly.length + degree];
        //Das Objekt Array wird an den Anfang kopiert
        System.arraycopy(poly, 0, result, 0, poly.length);
        //Rückgabe
        return new PolynomialGF2(result);
    }
}
