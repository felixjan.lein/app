package app.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * CloseListener Klasse horcht auf den Exit Button
 */
public class CloseListener implements ActionListener {
    /**
     * Wird ausgeführt, wenn Exit Knopf gedrückt
     * @param e event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}