package app.listener;

import app.display.AppDrawEvent;
import app.display.AutomatUpdater;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * PlusListener Klasse horcht auf den Start Button
 */
public class PlusListener implements ActionListener {
    /**
     * Referenz auf das errorLabel um Text schreiben zu können
     */
    private final JLabel errorLabel;
    /**
     * Referenz auf den Updater
     */
    private final AutomatUpdater automatUpdater;

    /**
     * Konstruktor für einen PlusListener
     * @param automatUpdater Referenz auf den Updater
     * @param errorLabel Referenz auf das errorLabel um Text schreiben zu können
     */
    public PlusListener(AutomatUpdater automatUpdater, JLabel errorLabel){
        this.automatUpdater = automatUpdater;
        this.errorLabel = errorLabel;
    }

    /**
     * Wird ausgeführt, wenn Original Knopf gedrückt
     * @param e event
     */
    @Override
    synchronized public void actionPerformed(ActionEvent e) {
        // Falls der automatUpdater gerade läuft -> Tu nichts
        if(this.automatUpdater.getState() == Thread.State.RUNNABLE){
            return;
        }
        // Falls die aktuelle Generation die Maximal erlaubte ist -> Tu nichts
        if(this.automatUpdater.getAutomat().getGeneration() == AppDrawEvent.GEN_LIMIT){
            errorLabel.setText("I can't go higher than " + AppDrawEvent.GEN_LIMIT + ".");
            return;
        }
        // Ansonsten update den Automaten mit einer Generation höher
        this.automatUpdater.update(this.automatUpdater.getAutomat().getGeneration() + 1);
    }
}
