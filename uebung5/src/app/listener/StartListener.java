package app.listener;

import app.display.AppDrawEvent;
import app.display.AutomatUpdater;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * StartListener Klasse horcht auf den Start Button
 */
public class StartListener implements ActionListener {
    /**
     * Referenz auf das errorLabel um Text schreiben zu können
     */
    private final JLabel errorLabel;
    /**
     * Referenz auf das Input Feld
     */
    private final JTextField numberInput;
    /**
     * Referenz auf den Updater
     */
    private final AutomatUpdater automatUpdater;

    /**
     * Konstruktor für einen StartListener
     * @param automatUpdater Referenz auf den Updater
     * @param numberInput Referenz auf das Input Feld
     * @param errorLabel Referenz auf das errorLabel um Text schreiben zu können
     */
    public StartListener(AutomatUpdater automatUpdater, JTextField numberInput, JLabel errorLabel){
        this.numberInput = numberInput;
        this.errorLabel = errorLabel;
        this.automatUpdater = automatUpdater;
    }

    /**
     * Wird ausgeführt, wenn Start Knopf gedrückt
     * @param e event
     */
    @Override
    synchronized public void actionPerformed(ActionEvent e) {
        // Falls es gerade läuft -> tu nichts
        if(this.automatUpdater.getState() == Thread.State.RUNNABLE){
            return;
        }
        // Falls der Input leer ist und der Updater gerade wartet -> Nachricht
        if(this.numberInput.getText().equals("") && this.automatUpdater.getState() == Thread.State.WAITING){
            this.errorLabel.setText("Try a number!");
            return;
        }
        // Versuche Rundenanzahl zu parsen
        int rounds;
        try {
            rounds = Integer.parseInt(this.numberInput.getText());
        } catch (NumberFormatException n){
            this.errorLabel.setText("Wrong input, only numbers allowed!");
            return;
        }

        // Falls Rundenanzahl kleiner 0 -> Error Nachricht
        // Falls Rundenanzahl höher als das Maximus -> Error Nachricht
        if(rounds < 0){
            this.errorLabel.setText("Try a new number higher than 0!");
            return;
        }else if (rounds > AppDrawEvent.GEN_LIMIT){
            this.errorLabel.setText("Sorry, but " + AppDrawEvent.GEN_LIMIT + " is the highest I can go.");
            return;
        }

        // Update den automatUpdater
        this.automatUpdater.update(rounds);
    }
}
