package app.listener;


import app.display.AutomatUpdater;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ResetListener Klasse horcht auf den Reset Button
 */
public class ResetListener implements ActionListener {
    /**
     * Referenz auf den AutomatUpdater
     */
    private final AutomatUpdater automatUpdater;

    /**
     * Konstruktor
     * @param automatUpdater Referenz auf den AutomatUpdater
     */
    public ResetListener(AutomatUpdater automatUpdater){
        this.automatUpdater = automatUpdater;
    }

    /**
     * Wird ausgeführt, wenn Reset Knopf gedrückt
     * @param e event
     */
    @Override
    synchronized public void actionPerformed(ActionEvent e) {
        // Wenn Der automatUpdater wartet, setze ihn auf Runde 0
        if(this.automatUpdater.getState() == Thread.State.WAITING){
            this.automatUpdater.update(0);
        }
    }
}
