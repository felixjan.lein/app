package app.display;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Eine Klasse, welche einen zellulären Automaten repräsentiert
 */
public class Automat{
    /**
     * Speichert die aktuelle Generation
     */
    private int generation;
    /**
     * Speichert den aktuellen Zustand des zellulären Automaten
     */
    private boolean[][] now;
    /**
     * Wird benötigt für die Berechnung der nächsten Generation
     */
    private boolean[][] next;
    /**
     * Bestimmt das padding um die Ursprungsgeneration
     */
    private final int padding;
    /**
     * Speichert die Länge (Höhe und Breite) des Automaten
     */
    private final int len;
    /**
     * Das Ursprungsbild für die erste Generation
     */
    private final BufferedImage image;

    /**
     * Konstruktor für einen Automaten
     * @param image Urspungbild
     * @param padding Abstand zum Rand
     */
    public Automat(BufferedImage image, int padding){
        this.image = getPatternBuf(image);
        this.padding = padding;
        this.init();
        this.len = this.now.length;
    }

    /**
     * init intitialisiert den Automaten
     * - Die Generation wird gesetzt
     * - Zwei neue Boolean Matrizen werden erstellt (now und next)
     * - Schwarze Pixel des umgewandelten Bildes werden zu lebenden Zellen
     */
    public void init(){
        this.generation = 0;

        int imageLen = this.image.getWidth();
        this.now = genQuadMatrix(imageLen+(2* this.padding *imageLen));
        this.next = genQuadMatrix(imageLen+(2* this.padding *imageLen));

        for (int i = 0; i < imageLen; i++) {
            for (int j = 0; j < imageLen; j++) {
                if(this.image.getRGB(j,i) == Color.BLACK.getRGB()){
                    this.now[i+(this.padding *imageLen)][j+(this.padding *imageLen)] = true;
                }
            }
        }
    }

    /**
     * Die Funktion setzt den Automaten auf seine nächste Generation
     * - Dabei wird ein performace Padding berechnet, damit nicht alles Pixel des
     *   Automaten neu berechnet werden müssen.
     * - Das Ergebnis wird in this.next geschrieben
     * - Nach Berechnung wird das Ergebnis in now kopiert und next neu aufgesetzt
     * - Die Generation wird erhöht
     */
    public void nextGeneration(){
        int perfPad = this.padding * this.image.getWidth() - this.generation - 1;
        for (int i = perfPad; i < this.len - perfPad; i++) {
            for (int j = perfPad; j < this.len - perfPad; j++) {
                int neighborCount = 0;
                for (int k = -1; k < 2; k++) {
                    for (int l = -1; l < 2; l++) {
                        if(this.now[(i+k+this.len)%this.len][(j+l+this.len)%this.len]){
                            if(!(k == 0 && l == 0)){
                                neighborCount++;
                            }
                        }
                    }
                }
                if (neighborCount % 2 != 0){
                    this.next[i][j] = true;
                }
            }
        }
        this.now = this.next.clone();
        this.next = genQuadMatrix(this.len);
        this.generation++;
    }

    /**
     * Die Funktion gibt die aktuelle Generation zurück
     * @return Generation
     */
    public int getGeneration(){
        return this.generation;
    }

    /**
     * Die Methode erzeugt eine neue Quadratische Boolean Matrix
     * @param len Seitenlängen
     * @return neue Boolean Matrix
     */
    private boolean[][] genQuadMatrix(int len){
        return new boolean[len][len];
    }

    /**
     * Die Funktion erstellt für den aktuellen Automaten ein BufferedImage.
     * Lebende Pixel bekommen Farbe, tote bleiben weiß
     * @return Ein neues Buffered Image, das Ergebnis
     */
    public BufferedImage toBuf(){
        BufferedImage result = new BufferedImage(this.len, this.len, BufferedImage.TYPE_INT_ARGB);
        for (int i = 0; i < this.len; i++) {
            for (int j = 0; j < this.len; j++) {
                if(this.now[i][j]){
                    result.setRGB(j, i, Color.decode("#52796f").getRGB());
                } else{
                    result.setRGB(j, i, Color.WHITE.getRGB());
                }
            }
        }
        return result;
    }

    /**
     * Die Funktion bekommt ein BufferedImage und wendet das Pattern darauf an.
     * Es wird skaliert und die 2*2 Pixel werden durch ein Pattern ersetzt.
     * @param img Das zu verändernde Bild
     * @return Ein neues Buffered Image, das Ergebnis
     */
    public static BufferedImage getPatternBuf(BufferedImage img){
        int w = img.getWidth();
        int h = img.getHeight();

        BufferedImage result = new BufferedImage(w*2, h*2, img.getType());

        // true == WHITE // false == BLACK
        boolean[][] pattern0 = {{false,false},{false,false}};
        boolean[][] pattern1 = {{true,false},{false,false}};
        boolean[][] pattern2 = {{false,true},{true,false}};
        boolean[][] pattern3 = {{false,true},{true,true}};
        boolean[][] pattern4 = {{true,true},{true,true}};

        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                int c = img.getRGB(x, y);
                Color color = new Color(c);
                int newC = (color.getRed() + color.getGreen() + color.getBlue()) / 3;

                if(newC >= 208){
                    applyPattern(pattern4, x, y, result);
                } else if(newC >= 156){
                    applyPattern(pattern3, x, y, result);
                } else if(newC >= 104){
                    applyPattern(pattern2, x, y, result);
                } else if(newC >= 52){
                    applyPattern(pattern1, x, y, result);
                } else{
                    applyPattern(pattern0, x, y, result);
                }
            }
        }
        return result;
    }

    /**
     * Die Funktion wendet ein übergebenes Pattern ein eine übergebene Stelle
     * eines übergebenen BufferedImages an, ersetzt also Pixel.
     * @param pattern Das Pattern, welches angewendet werden soll
     * @param x Die x-Koordinate
     * @param y Die y-Koordinate
     * @param result Das BufferedImage auf dem gearbeitet wird
     */
    private static void applyPattern(boolean[][] pattern, int x, int y, BufferedImage result){
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                if (pattern[i][j]){
                    result.setRGB(x*2 + i, y*2 + j, Color.WHITE.getRGB());
                }
            }
        }
    }
}
