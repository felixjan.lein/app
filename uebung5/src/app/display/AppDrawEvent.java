package app.display;

import app.listener.CloseListener;
import app.listener.PlusListener;
import app.listener.ResetListener;
import app.listener.StartListener;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Klasse für einen eigenen Button ohne blauen Schimmer
 */
class MyButton extends JButton {
    /**
     * Konstruktor für einen Button
     * @param text Display text
     */
    public MyButton(String text){
        super(text);
        this.setFocusPainted(false);
    }
}

/**
 * Neue Klasse für einen Frame mit einer DefaultCloseOperation
 */
class AppFrame extends JFrame {
    /**
     * Konstruktor für besagten Frame
     * @param title Titel des Fensters
     */
    public AppFrame(String title) {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

/**
 * Hauptklasse, welche die ausführbare Main-Methode enthällt
 */
public class AppDrawEvent {

    /**
     * Beschreibt die Maximalanzahl an Generationen
     */
    public static final int GEN_LIMIT = 256;
    /**
     * Der Default Input
     */
    private static final int DEFAULT_INPUT = 192;
    /**
     * Das Standardmäßige Padding des Automaten
     */
    public static final int PADDING_FACTOR = 4;

    /**
     * Main-Methode, welche ein Bild übergeben bekommt
     * @param args Pfad zu einem Bild
     */
    public static void main( String[] args ) {
        // Wenn nicht genau ein Parameter
        if(args.length != 1){
            throw new IllegalArgumentException("Exactly one image path as input allowed");
        }

        // Neues BufferedImage wird initialisiert
        final BufferedImage inputImage;
        final String path = args[0];
        try {
            inputImage = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        // Neuer Automat
        Automat automat = new Automat(inputImage, PADDING_FACTOR);


        // #### INITIALIZE FRAME AND PANELS ####
        JFrame frame = new AppFrame("Zellulärer Automat Visualisierung - Felix Lein");

        JPanel headerPanel = new JPanel(new BorderLayout());
        JPanel imagePanel = new JPanel(new BorderLayout());
        JPanel footerPanel = new JPanel(new BorderLayout());

        frame.add(headerPanel, BorderLayout.NORTH);
        frame.add(imagePanel, BorderLayout.CENTER);
        frame.add(footerPanel, BorderLayout.SOUTH);

        JPanel headerPanelLeft = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel headerPanelRight = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        headerPanel.add(headerPanelLeft, BorderLayout.WEST);
        headerPanel.add(headerPanelRight, BorderLayout.EAST);

        // #### IMAGE ####
        JLabel imageLabel = new JLabel(new ImageIcon(automat.toBuf()));
        imagePanel.add(imageLabel, BorderLayout.CENTER);


        // #### HEADER LEFT ####
        JLabel roundsLabel = new JLabel("Rounds:");
        JTextField numberInput = new JTextField(String.valueOf(DEFAULT_INPUT), 5);
        JButton start = new MyButton("Start");
        JLabel errorLabel = new JLabel("");

        headerPanelLeft.add(roundsLabel);
        headerPanelLeft.add(numberInput);
        headerPanelLeft.add(start);
        headerPanelLeft.add(errorLabel);


        // #### HEADER RIGHT ####
        JButton plus = new MyButton("+");
        plus.setPreferredSize(new Dimension(40, 27));

        headerPanelRight.add(plus);

        // #### FOOTER ####
        JButton exit = new MyButton("Exit");
        JButton reset = new MyButton("Reset");

        footerPanel.add(exit, BorderLayout.EAST);
        footerPanel.add(reset, BorderLayout.WEST);


        // Neuer Arbeiterthread
        AutomatUpdater automatUpdater = new AutomatUpdater(imageLabel, automat, errorLabel);
        automatUpdater.start();


        // #### BUTTON SETUP ####
        start.addActionListener(new StartListener(automatUpdater, numberInput, errorLabel));
        plus.addActionListener(new PlusListener(automatUpdater, errorLabel));
        reset.addActionListener(new ResetListener(automatUpdater));
        exit.addActionListener(new CloseListener());

        // #### FINALIZE ####
        frame.pack();
        frame.setVisible(true);
    }
}

