package app.display;


import javax.swing.*;

/**
 * Die Klasse Repräsentiert eine Automaten Updater
 * Sie kümmert sich ums darstellen der Generationen
 */
public class AutomatUpdater extends Thread {
    /**
     * Referenz auf den Automaten, der geupdated werden soll
     */
    private final Automat automat;
    /**
     * Referenz auf das imageLabel, wo das Ergebnis hingezeichnet wird
     */
    private final JLabel imageLabel;
    /**
     * Referenz auf das errorLabel um Text schreiben zu können
     */
    private final JLabel errorLabel;
    /**
     * Enthält die neue Rundenanzahl falls der Updater aufgerufen wird
     */
    private int newRound;


    /**
     * Konstruktor für einen neuen AutomatUpdater
     * @param imageLabel Wo hingezeichnet wird
     * @param automat Zu bearbeitender Automat
     * @param errorLabel Hier kommen die Benachrichtigungen
     */
    public AutomatUpdater(JLabel imageLabel, Automat automat, JLabel errorLabel){
        this.automat = automat;
        this.imageLabel = imageLabel;
        this.errorLabel = errorLabel;
    }

    /**
     * Die Funktion erzeugt newRound viele neue Generationen
     */
    public void iterate(){
        // Wurde newRound auf 0 gesetzt oder ist die neue Generationsanzahl
        // kleiner als die aktuelle, wird der Automat neugestartet
        if(this.automat.getGeneration() > this.newRound || this.newRound == 0) {
            this.automat.init();
            this.imageLabel.setIcon(new ImageIcon(this.automat.toBuf()));
        }
        // Es wird so oft eine neue Genration gebildet bis die Runden gleich sind
        while(automat.getGeneration() < this.newRound) {
            this.automat.nextGeneration();
            this.imageLabel.setIcon(new ImageIcon(this.automat.toBuf()));
            this.errorLabel.setText("Simulating " + this.automat.getGeneration() + ".".repeat(this.automat.getGeneration()/8 % 4));
        }
        this.errorLabel.setText("This is generation " + this.automat.getGeneration() + ".");
    }

    /**
     * run Methode, welche nur einmal beim Start des Threads aufgerufen wird
     */
    synchronized public void run(){
        while (true){
            this.iterate();
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Update setzt newRound auf den übergebenen Wert
     * und weckt dann den Thread wieder auf, das er beginnt zu rechnen
     * @param round Neue Anzahl Generationen
     */
    synchronized public void update(int round){
        this.newRound = round;
        this.notify();
    }

    /**
     * Die Funktion gibt eine Referenz auf den Automaten zurück
     * @return Der zu bearbeitende Automat
     */
    public Automat getAutomat(){
        return this.automat;
    }
}
