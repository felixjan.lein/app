package app.exercise.algebra;

/**
 * Die Klasse erbt von Rational und implementiert java.lang.Conparable,
 * somit sind einzelne Objekte miteinander vergleichbar
  */
public class CompRational extends Rational implements java.lang.Comparable<CompRational>{

    /**
     * Konstruktor mit zwei Parametern, ruft einfach den Konstruktor
     * der super-Klasse auf
     * @param numerator Zähler
     * @param denominator Nenner
     */
    public CompRational (long numerator, long denominator){
        super(numerator, denominator);
    }

    /**
     * Default konstruktor ruft auch den Konstruktor der Superklasse auf
     */
    public CompRational (){
        super();
    }

    /**
     * compareTo macht zwei Objekte der Klasse miteinander vergleichbar,
     * indem sie auf den gleichen nenner gebracht werden und der Zählr vergleicht
     * wird
     * @param o Das zu verlgeichende Objekt
     * @return kleiner 0, größer 0 , 0, wie die Objekte zueinander stehen
     */
    @Override
    public int compareTo(CompRational o) {
        long num1 = this.getN() * o.getD();
        long num2 = o.getN() * this.getD();
        return Long.compare(num1, num2);
    }
}
