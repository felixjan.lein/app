package app.exercise.algebra;

/**
 * Interface für Brüche
 */
public interface Fractional {
    /**
     * Getter Funktion
     * @return Zähler
     */
    long getN();

    /**
     * Getter Funktion
     * @return Nenner
     */
    long getD();

    /**
     * Funktion zum addieren zweier Fractionals
     * @param operand der andere Summand
     */
    void add(Fractional operand);

    /**
     * Funktion zum subtrahieren zweier Fractionals
     * @param operand der Subtrahent
     */
    void sub(Fractional operand);

    /**
     * Funktion zum multiplizieren zweier Fractionals
     * @param operand der andere Faktor
     */
    void mul(Fractional operand);

    /**
     * Funktion zum dividieren zweier Fractionals
     * @param operand der Divisor
     */
    void div(Fractional operand);

    /**
     * Funktion zum negieren eines Rationals
     * @return Neues Fractional: Negation
     */
    Fractional negation ();

    /**
     * Funktion zum Bilden des Kehrwerts
     * @return Neues Fractional: Kehrwert
     */
    Fractional reciprocal ();
}
