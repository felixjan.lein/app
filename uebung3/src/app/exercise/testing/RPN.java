package app.exercise.testing;

import app.exercise.algebra.Rational;
import java.util.Stack;

/**
 * Klasse, welche das Abarbeiten und auswerten von Ausdrücken in
 * umgekehrter polnische Notation mit Hilfe eines Stacks verwirklicht.
 * @author felix
 */
public class RPN {
    /**
     * Die Main-Methode arbeitet den Ausdruck ab und liefert ein Ergebnis
     * @param args Ein String-Array einzelner Zahlen und Operatoren
     */
    public static void main(String[] args) {
        //neuer Stack
        Stack<Rational> stack = new Stack<>();

        //Für jedes Element des Ausdrucks
        for (String s : args) {
            //Falls es eine nummer zwischen 0 und 9 ist
            if (checkForNumber(s)) {
                //kommt sie auf den Stack
                stack.push(new Rational(Long.parseLong(s), 1));
            } else {
                Rational operand2 = stack.pop();
                Rational operand1 = stack.pop();

                //Falls Operator, wird dieser auf die obersten beiden Objekte angewendet
                switch (s) {
                    case "+" -> operand1.add(operand2);
                    case "-" -> operand1.sub(operand2);
                    case "*" -> operand1.mul(operand2);
                    case "/" -> operand1.div(operand2);
                    //Falls ungültiges Zeichen
                    default -> {
                        System.out.println("Nur Operatoren +,-,*,/ erlaubt");
                        System.exit(-1);
                    }
                }
                //Ergebnis der Operation auf den Stack
                stack.push(operand1);

            }
        }
        //Ergebnisausgabe
        System.out.println(stack.pop());
    }

    /**
     * Die Methode guckt ob ein String zu einem Long geparsed werden kann
     * @param s ein String
     * @return boolean Wert ob möglich oder nicht
     */
    public static boolean checkForNumber(String s){
        //Falls es als Long interpretiert werden kann -> true, sonst -> false
        try {
            Long.parseLong(s);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }
}
