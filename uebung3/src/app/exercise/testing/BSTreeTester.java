package app.exercise.testing;

import app.exercise.adt.BSTree;
import app.exercise.algebra.CompRational;
import app.exercise.visualtree.RedBlackTreeDrawer;

import java.util.ArrayList;

/**
 * Klasse zum Testen unserer Funktionen
 */
public class BSTreeTester {


    /**
     * Statische Variable für kurze Zeit
     */
    public static int TIMESHORT = 150;
    /**
     * Statische Variable für mittlere Zeit
     */
    public static int TIMEMEDIUM = 800;
    /**
     * Statische Variable für lange Zeit
     */
    public static int TIMELONG = 2000;

    /**
     * Die Main Methode führt alle geforderten Tests aus
     * @param args Eingabe einzelner Long-Werte über die Kommandozeile
     */
    public static void main(String[] args) {
        //Check auf ungerade
        if (args.length % 2 != 0){
            throw new IllegalArgumentException("Error: odd number of Arguments!");
        }
        //Check auf Mindestlänge
        if (args.length < 2){
            throw new IllegalArgumentException("Error: I need at least one complete CompRational");
        }
        BSTree<CompRational> tree = new BSTree<>();
        BSTree<CompRational> subTreeOne = new BSTree<>();
        BSTree<CompRational> subTreeTwo = new BSTree<>();
        CompRational first = new CompRational();
        CompRational last= new CompRational();


        RedBlackTreeDrawer<CompRational> visual = new RedBlackTreeDrawer<>();
        update(tree, visual, "Tree will be build!", TIMELONG);

        //Bearbeiten der Eingabe
        for (int i = 0; i < args.length; i+=2) {
            long numerator, denominator;
            //parse Versuch
            try{
                numerator = Long.parseLong(args[i]);
                denominator = Long.parseLong(args[i+1]);
            } catch (IllegalArgumentException e){
                System.out.println("Error: Only numbers as input allowed");
                e.printStackTrace();
                return;
            }
            //Erstellen und hinzufügen des Knotens
            CompRational newNode = new CompRational(numerator, denominator);
            tree.add(newNode);
            update(tree, visual, "Node = " + newNode + " added.", TIMESHORT);
            //Zu den Sub-Bäumen hinzufügen
            if (i % 4 == 0){
                subTreeOne.add(newNode);
            } else {
                subTreeTwo.add(newNode);
            }
            //First und Last
            if (i == 0){
                first.setND(numerator, denominator);
            } else if (i == args.length - 2){
                last.setND(numerator, denominator);
            }
        }



        System.out.println("tree = " + tree);
        update(tree, visual, "Our tree", TIMELONG);

        System.out.println("sub-tree-one = " + subTreeOne);
        update(subTreeOne, visual, "Subtree 1", TIMELONG);

        System.out.println("sub-tree-two = " + subTreeTwo);
        update(subTreeTwo, visual,"Subtree 2", TIMELONG);

        System.out.println("sub-tree-one in tree? " + tree.containsAll(subTreeOne));
        System.out.println("sub-tree-two in tree? " + tree.containsAll(subTreeTwo));

        System.out.println("First CR = " + first + " in tree? " + tree.contains(first));
        System.out.println("Last CR = " + last + " in tree? " + tree.contains(last));

        update(tree, visual,"Our tree", TIMELONG);

        tree.remove(first);
        System.out.println("tree without first " + first + " = " + tree);
        update(tree, visual,"First Node " + first + " removed", TIMELONG);

        tree.remove(last);
        System.out.println("tree without last " + last + " = " + tree);
        update(tree, visual,"Last Node " + last + " removed", TIMELONG);


        ArrayList<CompRational> testNumbers = getNumbers(100, (CompRational) tree.toArray()[0], (CompRational) tree.toArray()[tree.toArray().length - 1]);

        //Testen ob die generierten Knoten enthalten sind
        for (CompRational c: testNumbers) {
            if(tree.contains(c)){
                System.out.println("Tree contains = " + c + ", delete it.");
                tree.remove(c);

                System.out.println(tree);
                update(tree, visual,"Node " + c + " removed", TIMEMEDIUM);
            }
        }

        update(tree, visual,"Final tree", TIMELONG);
    }

    /**
     * Die Funktion liefert CompRationals zwischen zwei Grenzen zurück
     * @param amount Anzahl an CompRationals
     * @param lowest Untere Grenze
     * @param highest Obere Grenze
     * @return ArrayList von CompRationals
     */
    public static ArrayList<CompRational> getNumbers(int amount, CompRational lowest, CompRational highest){
        ArrayList<CompRational> result = new ArrayList<>();
        int counter = 0;
        int numberCount = 0;
        while (counter < amount){
            long numerator = 1 + (int)(Math.random() * ((100 - 1) + 1));
            long denominator = 1 + (int)(Math.random() * ((100 - 1) + 1));
            CompRational newFraction = new CompRational(numerator, denominator);
            if(newFraction.compareTo(lowest) > 0 && newFraction.compareTo(highest) < 0){
                result.add(newFraction);
                counter++;
            }
            numberCount++;
        }
        System.out.println("Had to generate " + numberCount + " Fractions for "+ amount + " in the range");
        return result;
    }

    /**
     * Unterbricht das Programm für eine bestimmte Anzahl an Millisekunden
     * @param ms Millisekunden
     */
    public static void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Die Funktion kümmert sich ums darstellen der Bäume
     * @param tree Der zu malende Baum
     * @param visual Referenz auf den TreeDrawer
     * @param text Titel des Drawers
     * @param time Wie lange soll es erscheinen
     */
    public static void update(BSTree<CompRational> tree, RedBlackTreeDrawer<CompRational> visual, String text, int time){
        visual.draw(tree.getRoot());
        visual.setTitle(text);
        wait(time);
    }
}
