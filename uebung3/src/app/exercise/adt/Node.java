package app.exercise.adt;

import app.exercise.visualtree.DrawableTreeElement;

/**
 * Klasse um Knoten eines Baumes zu erstellen
 * @param <T> Der Inhalts-Type des Baumes
 */
public class Node<T> implements DrawableTreeElement<T> {

    /**
     * Objektvariable ob der Knoten rot ist
     */
    private final boolean red;
    /**
     * Objektvariable des Wertes des Knotens
     */
    private T val;
    /**
     * Objektvariable rechter Kind-Knoten
     */
    private Node<T> right;
    /**
     * Objektvariable linker Kind-Knoten
     */
    private Node<T> left;
    /**
     * Objektvariable Eltern-Knoten
     */
    private Node<T> parent;

    /**
     * Konstrutor eines Knotens
     * @param val Der Wert des Knotens
     * @param parent Woran der Knoten gehängt werden soll
     */
    public Node(T val, Node<T> parent){
        this.val = val;
        this.parent = parent;
        this.red = Math.random() <= 0.5;
        this.left = this.right = null;
    }

    /**
     * Getter Funktion fürs linke Kind
     * @return Der linke Kinder-Knoten
     */
    @Override
    public DrawableTreeElement<T> getLeft() {
        return this.left;
    }

    /**
     * Getter Funktion fürs rechte Kind
     * @return Der rechte Kinder-Knoten
     */
    @Override
    public DrawableTreeElement<T> getRight() {
        return this.right;
    }

    /**
     * Setter Funktion fürs rechte Kind
     * @param node Der Knoten, der das Kind werden soll
     */
    public void setRight(Node<T> node){
        this.right = node;
    }

    /**
     * Setter Funktion fürs linke Kind
     * @param node Der Knoten, der das Kind werden soll
     */
    public void setLeft(Node<T> node){
        this.left = node;
    }

    /**
     * Setter Funktion fürs setzen des Wertes
     * @param val Der Wert
     */
    public void setVal(T val) {
        this.val = val;
    }

    /**
     * Setter Funktion um den Parent-Knoten festzulegen
     * @param node Der Parent-Knoten
     */
    public void setParent(Node<T> node) {
        this.parent = node;
    }

    /**
     * Auskunft ob der Knoten rot ist oder nicht
     * @return Boolean Wert über die Aussage
     */
    @Override
    public boolean isRed() {
        return this.red;
    }

    /**
     * Getter Funktion für den Wert des Knotens
     * @return Wert des Knotens
     */
    @Override
    public T getValue() {
        return this.val;
    }

    /**
     * Die Funktion gibt die Referenz auf den Parent-Knoten
     * @return Parent-Knoten
     */
    public Node<T> getParent() {
        return parent;
    }
}
