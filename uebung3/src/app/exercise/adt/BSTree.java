package app.exercise.adt;

import app.exercise.algebra.CompRational;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * Klasse, mit der sich ein BSTree realisieren lässt
 * @param <E> der zu bearbeitende Type
 */
public class BSTree<E> extends java.util.AbstractCollection<E> {
    /**
     * Objektvariable, welche die Wurzel des Baums speichert
     */
    private Node<E> root;

    /**
     * Funktion, zum hinzufügen eines Knotens
     * Sie ruft eine weitere Hilfsfunktion auf
     * @param e Element des Typs des Baumes
     * @return Boolean ob das Einfügen erfolgreich war
     */
    public boolean add(E e){
        return add(root, e);
    }

    /**
     * Eigentliche Funktion, welche rekursiv einen Knoten einfügt
     * @param node Die Wurzel
     * @param elem Das Element zum hinzufügen
     * @return Boolean ob das Einfügen erfolgreich war
     */
    private boolean add(Node<E> node, E elem){
        //Falls erster Knoten, wird er root
        if (root == null){
            root = new Node<>(elem, null);
            return true;
        }

        //Falls Element bereits im Baum
        if (this.contains(elem)) return false;

        //checken, welcher Knoten größer ist
        CompRational nodeValue = (CompRational) node.getValue();
        int whatDirection = nodeValue.compareTo((CompRational) elem);

        //Links einfügen
        if(whatDirection > 0){
            if(node.getLeft() == null){
                node.setLeft(new Node<>(elem, node));
            } else{
                add((Node<E>) node.getLeft(), elem);
            }
            return true;
        //Rechts einfügen
        } else if (whatDirection < 0){
            if (node.getRight() == null) {
                node.setRight(new Node<>(elem, node));
            } else {
                add((Node<E>) node.getRight(), elem);
            }
            return true;
        }
        return false;
    }

    /**
     * Funktion zum löschen eines Knotens
     * Sie ruft eine andere Funktion auf
     * @param o Das zu löschende Objekt
     * @return Boolean ob Löschung erfolgreich
     */
    public boolean remove(Object o){
        return remove(root, o);
    }

    /**
     * Funktion zum rekursiven Löschen
     * @param node Die Wurzel des Baumes
     * @param o Der zu löschende Wert
     * @return Boolean ob das Löschen erfolgreich war
     */
    public boolean remove(Node<E> node, Object o){
        //wenn der Baum leer ist oder der Knoten nicht vorhanden kann nichts gelöscht werden
        if(root == null || !this.contains(o)) return false;

        CompRational nodeValue = (CompRational) node.getValue();
        int whatDirection = nodeValue.compareTo((CompRational) o);

        //Weiter runter im Baum
        if(whatDirection > 0){
            remove((Node<E>) node.getLeft(), o);
        } else if (whatDirection < 0){
            remove((Node<E>) node.getRight(), o);
        //Wurde der Knoten gefunden wird gelöscht
        } else{
            //Fall 1: Knoten hat keine Kidner
            if (node.getLeft() == null && node.getRight() == null){
                if (node.getParent().getLeft() != null && node.getParent().getLeft().equals(node)){
                    node.getParent().setLeft(null);
                } else if (node.getParent().getRight() != null && node.getParent().getRight().equals(node)){
                    node.getParent().setRight(null);
                }
            //Fall 2.1: Knoten hat nur ein rechtes Kind
            } else if (node.getLeft() == null && node.getRight() != null){
                if (node == root){
                    root = (Node<E>) node.getRight();
                } else if (node.getParent().getLeft().equals(node)){
                    node.getParent().setLeft((Node<E>) node.getRight());
                } else if (node.getParent().getRight().equals(node)){
                    node.getParent().setRight((Node<E>) node.getRight());
                }
                ((Node<E>) node.getRight()).setParent(node.getParent());
            //Fall 2.2: Knoten hat nur ein linkes Kind
            } else if (node.getRight() == null && node.getLeft() != null) {
                if (node == root){
                    root = (Node<E>) node.getLeft();
                } else if (node.getParent().getLeft().equals(node)){
                    node.getParent().setLeft((Node<E>) node.getLeft());
                } else if (node.getParent().getRight().equals(node)){
                    node.getParent().setRight((Node<E>) node.getLeft());
                }
                ((Node<E>) node.getLeft()).setParent(node.getParent());
            //Fall 3: Knoten hat 2 Kinder
            } else{
                E smallestKey = getSmallestKey((Node<E>) node.getRight());
                node.setVal(smallestKey);
                remove((Node<E>) node.getRight(), smallestKey);
            }
        }
        return true;
    }

    /**
     * Die Funktion findet den kleinste Value eines Subtree von Knoten
     * @param root Die Wurzel des zu übergebenden Subtrees
     * @return Der kleinste Wert
     */
    public E getSmallestKey(Node<E> root){
        if(root.getLeft() == null){
            return root.getValue();
        } else{
            return getSmallestKey((Node<E>) root.getLeft());
        }
    }

    /**
     * Die Funktion gibt die Wurzel eines Baumes zurück
     * @return Root-Node
     */
    public Node<E> getRoot(){
        return this.root;
    }

    /**
     * Funktion zum Erzeugen eines Iterators
     * @return Ein Iterator über dem jetzigen Baum
     */
    @Override
    public Iterator<E> iterator() {
        return new BSTreeIterator<>(this);
    }

    /**
     * Die Funktion guckt ob ein Wert im Baum vorhanden ist
     * @param o Gesuchter Wert
     * @return Boolean ob Wert vorhanden
     */
    public boolean contains(Object o){
        for (E e: this) {
            if(o.equals(e)) return true;
        }
        return false;
    }

    /**
     * Die Funktion gibt die Anzahl der Knoten eines Baumes zurück
     * @return Integer der Größe
     */
    @Override
    public int size() {
        int size = 0;
        for (E e : this) size++;
        return size;
    }

    /**
     * Die Funktion macht es möglich zu testen, ob mehrere Werte im Baum
     * vorhanden sind.
     * @param c Eine Collection an Elementen des richtigen Typs
     * @return Boolean, ob alle enthalten
     */
    public boolean containsAll(Collection<?> c){
        for (Object o : c) if (!this.contains(o)) return false;
        return true;
    }

    /**
     * Funktion guckt ob der Baum leer ist
     * @return Boolean ob Baum leer
     */
    @Override
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * Schönere Ausgabe des Baumes
     * @return Der Baum als aufsteigender String
     */
    @Override
    public String toString() {
        return Arrays.toString(this.toArray()) + " root=" + root.getValue();
    }

    /**
     * Der Baum wird zu einem Array
     * @return Die Werte des Baumes in einem aufsteigenden Array
     */
    public Object[] toArray(){
        Object[] array = new Object[size()];
        int i = 0;
        for (E e: this) array[i++] = e;
        return array;
    }
}
