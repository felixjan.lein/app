package app.exercise.adt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

/**
 * Iterator für einen Binary-Search-Tree
 * @param <E> Ein Type
 */
public class BSTreeIterator<E> implements Iterator<E> {

    /**
     * Objektvariable index speichert den jetzigen Index
     */
    private int index = 0;
    /**
     * Objektvariable nodeList speicherte die Liste der Knoten
     */
    private final List<Node<E>> nodeList;

    /**
     * Der Konstruktor setzt die Liste des Iterators
     * @param tree Der Baum für den der Iterator erstellt werden soll
     */
    public BSTreeIterator(BSTree<E> tree){
        this.nodeList = getOrderedList((Node<E>) tree.getRoot());
    }

    /**
     * Methode zum erzeugen einer Liste
     * @param root Die Wurzel des Baumes
     * @return Die Liste der Knoten
     */
    private List<Node<E>> getOrderedList(Node<E> root){

        List<Node<E>> list = new ArrayList<>();

        if (root.getLeft() != null){
            list.addAll(getOrderedList((Node<E>) root.getLeft()));
        }
        list.add(root);
        if (root.getRight() != null){
            list.addAll(getOrderedList((Node<E>) root.getRight()));
        }
        return list;
    }

    /**
     * Funktion überprüft ob der Iterator ein nächstes Element hat
     * @return Boolean ob das der Fall ist
     */
    @Override
    public boolean hasNext() {
        return index < nodeList.size();
    }


    /**
     * Funktion zum weiter iterieren
     * @return Der Wert des nächsten Knotens
     */
    @Override
    public E next() {
        Node<E> node = nodeList.get(index++);
        return node.getValue();
    }

    /**
     * Diese Funktion ist nicht implementiert
     * @param action idk
     */
    @Override
    public void forEachRemaining(Consumer<? super E> action) {
        throw new UnsupportedOperationException("forEachRemaining() ist nicht definiert");
    }

    /**
     * Diese Funktion ist nicht implementiert
     */
    @Override
    public void remove() {
        throw new UnsupportedOperationException("remove() ist nicht definiert");
    }
}
